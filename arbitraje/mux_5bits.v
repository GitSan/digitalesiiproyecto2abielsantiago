// +FHDR------------------------------------------------------------
//                 Copyright (c) 2018 .
//                       ALL RIGHTS RESERVED
// -----------------------------------------------------------------
// Filename      : muxL2_w44p6.v
// Author        : 
// Created On    : 2018-11-03 18:49
// Last Modified : 
// -----------------------------------------------------------------
// Description:
//            n-intento de lograr un mux 2 a 1
//
// -FHDR------------------------------------------------------------


`timescale 1ns/100ps

module mux_5bits (
    input clk_f,
    input clk_2f,
    input clk_sel,
    input reset_L,
    input [4:0] data_0,
    input [4:0] data_1,
// by  2018-11-21 |     input valid_data_0,
// by  2018-11-21 |     input valid_data_1,
    output reg [4:0] data_out
// by  2018-11-21 |     output reg valid_data_out_mL2
);

reg [4:0] in_data_0, in_data_1;
// by  2018-11-21 | reg in_valid_data_0, in_valid_data_1;
wire selector;

wire[4:0] mux_0_data;
// by  2018-11-21 | wire mux_0_valid;

reg [4:0] mux_out_data_0;
// by  2018-11-21 | reg valid_mux_out_data_0;

// by  2018-11-21 | wire valid_out, valid_both, valid_one, valid_select;
// by  2018-11-21 | wire [4:0] data_entry_select, data_valid_select, data_entry_valid, data_valid_out;
wire [4:0] data_mux_out;


// by  2018-11-07 | always @(posedge clk_f)
always @(*)
begin
    if(reset_L)
    begin
        in_data_0 = data_0;
        in_data_1 = data_1;
// by  2018-11-21 |         in_valid_data_0 = valid_data_0;
// by  2018-11-21 |         in_valid_data_1 = valid_data_1;
    end
    else
    begin
        in_data_0 = 5'b0;
        in_data_1 = 5'b0;
// by  2018-11-21 |         in_valid_data_0 = 1'b0;
// by  2018-11-21 |         in_valid_data_1 = 1'b0;
    end
end

assign selector = clk_sel;


// by  2018-11-21 | assign valid_out = in_valid_data_0 | in_valid_data_1;
// by  2018-11-21 | assign valid_both = in_valid_data_0 & in_valid_data_1;
// by  2018-11-21 | assign valid_one = in_valid_data_0 ^ in_valid_data_1;
// by  2018-11-21 | assign valid_select = valid_one & in_valid_data_1; 
// by  2018-11-21 | assign data_valid_out[4:0] = (valid_out)?data_entry_valid[4:0]:data_out_mL2[4:0];
// by  2018-11-21 | assign data_entry_valid[4:0] = (valid_both)?data_entry_select[4:0]: data_valid_select[4:0];
// by  2018-11-21 | assign data_valid_select[4:0] = (valid_select)?in_data_1[4:0]:in_data_0[4:0];
// by  2018-11-21 | assign data_entry_select[4:0] = (selector)?in_data_1[4:0]:in_data_0[4:0];
	assign data_mux_out = (selector)?in_data_1:in_data_0;

always @(posedge clk_2f)
begin
    if (reset_L)
    begin
// by  2018-11-21 |         valid_data_out_mL2 <= valid_out;
        data_out <= data_mux_out ;
    end
    else
    begin
        data_out <= 5'b0;
// by  2018-11-21 |         valid_data_out_mL2 <= 1'b0;
    end
end

endmodule
