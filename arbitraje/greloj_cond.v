// Trabajando a 16 ciclos de reloj

// va a haber un reloj de entrada -> clk_in
// van a haber tres relojes de salida -> clk_out_4, clk_out_2 y clk_out_1

module greloj_cond(
    output reg clk_8,
    output reg clk_4,
    output reg clk_2,
    output reg clk_1,
    input reset_L,
    input clk_in);

reg contador_4;  // contador de bits para la frecuencia de salida de 4 ciclos reloj
reg [1:0] contador_2;  // contador de bits para la frecuencia de salida de 2 ciclos reloj
reg [2:0] contador_1;  // contador de bits para la frecuencia de salida de 1 ciclos reloj

// Salida de 8 ciclos reloj
// 16 / 2 = 4 ciclos reloj
always @(posedge clk_in) begin
    clk_8 <= 1;
    if(reset_L)
    begin
            clk_8 <= ~clk_8;  // cambia la señal de salida con la nueva frecuencia
    end
end

// Salida de 4 ciclos reloj
// 16 / 4 = 4 ciclos reloj
always @(posedge clk_in) begin
    contador_4 <= 0;
    clk_4 <= 1;
    if(reset_L)
    begin
        contador_4 <= ~contador_4;  // suma uno al contador de bits
        if (contador_4 == 1) begin  // si el contador es igual a 2bits procede a dividir
            clk_4 <= ~clk_4;  // cambia la señal de salida con la nueva frecuencia
        end
        else begin
            clk_4 <= clk_4;  // mantiene la señal de salida de no cumplirse el contador
        end
    end
end

// Salida de 2 ciclos reloj
// 16 / 8 = 2 ciclos reloj
always @(posedge clk_in) begin
    contador_2 <= 0;
    clk_2<=1;
    if(reset_L)
    begin
        contador_2 <= contador_2 + 1;  // suma uno al contador de bits
        if(contador_2 == 3) begin  // si el contador es igual a 4bits procede a dividir
            clk_2 <= ~clk_2;  // cambia la señal de salida con la nueva frecuencia
            //contador_2 <= 0;  // reinicia cuenta del contador
        end
        else begin
            clk_2 <= clk_2;  // mantiene la señal de salida de no cumplirse el contador
        end
    end
end

// Salida de 1 ciclos reloj
// 16 / 16 = 1 ciclos reloj
always @(posedge clk_in) begin
    contador_1 <= 0;
    clk_1<=1;
    if(reset_L)
    begin
        contador_1 <= contador_1 + 1;  // suma uno al contador de bits
        if(contador_1 == 7) begin  // si el contador es igual a 8bits procede a dividir
            clk_1 <= ~clk_1;  // cambia la señal de salida con la nueva frecuencia
           // contador_1 <= 0;  // reinicia cuenta del contador
        end
        else begin
            clk_1 <= clk_1;  // mantiene la señal de salida de no cumplirse el contador
        end
    end
end

endmodule
