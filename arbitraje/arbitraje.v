`include "roundrobin.v"
`include "mux_5bits.v"
`include "mux_6bits.v"
`include "demux_5bits.v"

module arbitraje (
	//señales de entrada
	input clk,
	input clk_2f,
	input rst,
	//VC0 del puerto de entrada 0
	input [4:0] data_fifo_VC0E0,
	input empty_VC0E0,
	output pop_req_VC0E0,
	//VC1 del puerto de entrada 0
	input [4:0] data_fifo_VC1E0,
	input empty_VC1E0,
	output pop_req_VC1E0,
	//VC0 del puerto de entrada 1
	input [4:0] data_fifo_VC0E1,
	input empty_VC0E1,
	output pop_req_VC0E1,
	//VC1 del puerto de entrada 1
	input [4:0] data_fifo_VC1E1,
	input empty_VC1E1,
	output pop_req_VC1E1,
	//puertos de salida final
	output reg [4:0] puerto_salida_0,
	output reg [4:0] puerto_salida_1,
    output reg valid_puerto_salida_0,
	output reg valid_puerto_salida_1
);

	//para la logica al inicio del modulo
	wire and1;		//and entre VC0E0 y VC0E1
	wire and2;		//and entre and1 y not VC1E0, va al request 0 del roundrobin VC1
	wire and3;		//and entre and1 y not VC1E1, va al request 1 del roundrobin VC1
	wire req0_VC0, req1_VC0, req0_VC1, req1_VC1;

	//para roundrobin
	wire valid_rr_VC0, id_rr_VC0, valid_rr_VC1, id_rr_VC1;
	//para muxes VC0 y VC1
	wire [4:0] data_out_VC0, data_out_VC1;
	wire valid_data_out_VC0, valid_data_out_VC1;
	reg log_valid_valid_mux_out;
	//para demux
	wire [4:0] demux_out_0, demux_out_1;
	wire valid_mux_out, valid_demux_out_0, valid_demux_out_1;

	//para concatenacion
	wire [5:0] mux6_VC0, mux6_VC1;
	wire [5:0] mux_out;
	wire [4:0] demux5;
	wire dest;

	//logica combinacional
	assign req0_VC0 = !empty_VC0E0;	//para el roundrobin de VC0
	assign req1_VC0 = !empty_VC0E1;
	assign and1 = empty_VC0E0 & empty_VC0E1;
	assign and2 = and1 & !empty_VC1E0;
	assign and3 = and1 & !empty_VC1E1;
	assign req0_VC1 = and2;
	assign req1_VC1 = and3;

	//roundrobin VC0 y VC1
	roundrobin rrVC0 (clk, req0_VC0, req1_VC0, valid_rr_VC0, id_rr_VC0);	//roundrobin del VC0
	roundrobin rrVC1 (clk, req0_VC1, req1_VC1, valid_rr_VC1, id_rr_VC1);	//roundrobin del VC1

	//logica pop VC0s y VC1s
	//debe agregarse el caso de losreset que no mande pop
	assign pop_req_VC0E0 = (rst && valid_rr_VC0)?((id_rr_VC0)?0:1):0;	//
	assign pop_req_VC0E1 = (rst && valid_rr_VC0)?((id_rr_VC0)?1:0):0;	//
	assign pop_req_VC1E0 = (rst && valid_rr_VC1)?((id_rr_VC1)?0:1):0;	//
	assign pop_req_VC1E1 = (rst && valid_rr_VC1)?((id_rr_VC1)?1:0):0;	//

	//logica port mux VC0 y mux VC1
	mux_5bits muxVC0 (clk, clk_2f, id_rr_VC0, rst, data_fifo_VC0E0[4:0], data_fifo_VC0E1[4:0], data_out_VC0[4:0]);	//mux del lado VC0
	mux_5bits muxVC1 (clk, clk_2f, id_rr_VC1, rst, data_fifo_VC1E0[4:0], data_fifo_VC1E1[4:0], data_out_VC1[4:0]);	//mux del lado VC1

    //logica de concatenacion para MUX
	assign mux6_VC0[5:0] = {1'b0, data_out_VC0[4:0]};
	assign mux6_VC1[5:0] = {1'b1, data_out_VC1[4:0]};

	//logica valid mux VC0 y mux VC1
	//revisar porque el valid está adelantado respecto al dato
	//es decir, hay que meterle un flop al valid
	mux_6bits mux (clk, clk_2f, !valid_rr_VC0, rst, mux6_VC0[5:0], mux6_VC1[5:0], valid_rr_VC0, valid_rr_VC1, mux_out[5:0], valid_mux_out);	//mux final  
	

    //logica de concatenacion para deMUX
	assign demux5[4:0] = {mux_out[5],mux_out[3:0]};
	assign dest = mux_out[4];

	//demux al final del modulo
	//falta arreglar de aqui en adelante.
	demux_5bits demux (dest, clk_2f, rst, demux_out_0[4:0], valid_demux_out_0, valid_demux_out_1, demux_out_1[4:0], log_valid_valid_mux_out, demux5[4:0]);

	//append final
	//esta @2f, podria necesiar otra etapade flops para sincronizar
	always @(posedge clk_2f) begin
			puerto_salida_0 <= 0;	//el vc_id es
			puerto_salida_1 <= 0;	//el vc_id es
			valid_puerto_salida_0 <= 0;
			valid_puerto_salida_1 <= 0;
			log_valid_valid_mux_out <= 0;
		if (rst) begin
			puerto_salida_0 <= demux_out_0;	//el vc_id es
			puerto_salida_1 <= demux_out_1;	//el vc_id es
			valid_puerto_salida_0 <= valid_demux_out_0;
			valid_puerto_salida_1 <= valid_demux_out_1;
			log_valid_valid_mux_out <= valid_mux_out;
		end
	end

endmodule
