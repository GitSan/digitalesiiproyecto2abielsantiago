module arbitraje_p (
	//señales de entrada
	output reg clk,
	output reg rst,
	output reg rst_clk,
	//VC0 del puerto de entrada 0
	output reg [4:0] data_fifo_VC0E0,
	output reg empty_VC0E0,
	input pop_req_VC0E0_c,
	input pop_req_VC0E0_e,
	//VC1 del puerto de entrada 0
	output reg [4:0] data_fifo_VC1E0,
	output reg empty_VC1E0,
	input pop_req_VC1E0_c,
	input pop_req_VC1E0_e,
	//VC0 del puerto de entrada 1
	output reg [4:0] data_fifo_VC0E1,
	output reg empty_VC0E1,
	input pop_req_VC0E1_c,
	input pop_req_VC0E1_e,
	//VC1 del puerto de entrada 1
	output reg [4:0] data_fifo_VC1E1,
	output reg empty_VC1E1,
	input pop_req_VC1E1_c,
	input pop_req_VC1E1_e,
	//puertos de salida final
	input [4:0] puerto_salida_0_c,
	input [4:0] puerto_salida_1_c,
    input valid_puerto_salida_0_c,
	input valid_puerto_salida_1_c,
	input [4:0] puerto_salida_0_e,
	input [4:0] puerto_salida_1_e,
    input valid_puerto_salida_0_e,
	input valid_puerto_salida_1_e
);


	//Reloj
	initial clk <= 0;
	always #1 clk <= ~clk;

    //Declaración de variables internas
    reg compare;
    reg [9:0] conductual, estructural;

    initial begin
        $dumpfile("arbitraje.vcd");
        $dumpvars;
		rst <= 0;
		rst_clk <= 0;
		data_fifo_VC0E0 <=5'h0;
		data_fifo_VC1E0 <=5'h0;
		data_fifo_VC0E1 <=5'h0;
		data_fifo_VC1E1 <=5'h0;
        empty_VC0E0<=0;
        empty_VC1E0<=0;
        empty_VC0E1<=0;
        empty_VC1E1<=0;
	    #10 @(posedge clk);
		rst_clk <= 1;
	    #10 @(posedge clk);
		rst<=1;
		data_fifo_VC0E0 <=5'h1A;
		data_fifo_VC1E0 <=5'h1F;
		data_fifo_VC0E1 <=5'h18;
		data_fifo_VC1E1 <=5'h05;
        empty_VC1E0<=1;
		@(posedge clk);@(posedge clk);
        empty_VC1E1<=1;
		@(posedge clk);@(posedge clk);
        empty_VC0E1<=1;
		#20 @(posedge clk);@(posedge clk);
        empty_VC0E0<=1;
		#20 @(posedge clk);@(posedge clk);
        empty_VC1E1<=0;
		#10 @(posedge clk);@(posedge clk);
        empty_VC1E0<=0;
		#20 @(posedge clk);@(posedge clk);
        empty_VC1E1<=1;
		#10 @(posedge clk);@(posedge clk);
        empty_VC1E0<=1;
		#20 $finish;
	end

	//Asignación de salidas de los multiplexores a registros internos de probador.v para comparación
    always @(posedge clk)
    begin
        conductual[9:0] <= {valid_puerto_salida_0_c,valid_puerto_salida_1_c,puerto_salida_0_c[4:0],puerto_salida_1_c[4:0]};
        estructural[9:0] <= {valid_puerto_salida_0_e,valid_puerto_salida_1_e,puerto_salida_0_e[4:0],puerto_salida_1_e[4:0]};
    end
    //Lógica de comparación
    always @(*)
    begin
        compare = 0;
        if(conductual != estructural)
        begin
            compare = 1;
        end
    end

endmodule
