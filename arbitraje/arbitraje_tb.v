`include "arbitraje.v"
`include "arbitraje_p.v"
`include "arbitraje_estructural.v"
`include "cmos_cells.v"
`include "greloj_cond.v"
`timescale 1ns/100ps

module arbitraje_tb();

    wire clk_1f, clk_2f, clk_4f, clk_8f, clk_16f, rst, rst_clk, empty_VC0E0, empty_VC1E0, empty_VC0E1, empty_VC1E1;
	wire pop_req_VC0E0_c, pop_req_VC1E0_c, pop_req_VC0E1_c, pop_req_VC1E1_c, valid_puerto_salida_0_c, valid_puerto_salida_1_c;
	wire pop_req_VC0E0_e, pop_req_VC1E0_e, pop_req_VC0E1_e, pop_req_VC1E1_e, valid_puerto_salida_0_e, valid_puerto_salida_1_e;
	wire [4:0] data_fifo_VC0E0, data_fifo_VC1E0, data_fifo_VC0E1, data_fifo_VC1E1;
	wire [4:0] puerto_salida_0_c, puerto_salida_1_c;
	wire [4:0] puerto_salida_0_e, puerto_salida_1_e;

    greloj_cond reloj (/*AUTOINST*/
		       // Outputs
		       .clk_8		(clk_8f),
		       .clk_4		(clk_4f),
		       .clk_2		(clk_2f),
		       .clk_1		(clk_1f),
		       // Inputs
		       .reset_L		(rst_clk),
		       .clk_in		(clk_16f));

	arbitraje conductual (/*AUTOINST*/
			      // Outputs
			      .pop_req_VC0E0	(pop_req_VC0E0_c),
			      .pop_req_VC1E0	(pop_req_VC1E0_c),
			      .pop_req_VC0E1	(pop_req_VC0E1_c),
			      .pop_req_VC1E1	(pop_req_VC1E1_c),
			      .puerto_salida_0	(puerto_salida_0_c[4:0]),
			      .puerto_salida_1	(puerto_salida_1_c[4:0]),
			      .valid_puerto_salida_0(valid_puerto_salida_0_c),
			      .valid_puerto_salida_1(valid_puerto_salida_1_c),
			      // Inputs
			      .clk		(clk_8f),
			      .clk_2f		(clk_16f),
			      .rst		(rst),
			      .data_fifo_VC0E0	(data_fifo_VC0E0[4:0]),
			      .empty_VC0E0	(empty_VC0E0),
			      .data_fifo_VC1E0	(data_fifo_VC1E0[4:0]),
			      .empty_VC1E0	(empty_VC1E0),
			      .data_fifo_VC0E1	(data_fifo_VC0E1[4:0]),
			      .empty_VC0E1	(empty_VC0E1),
			      .data_fifo_VC1E1	(data_fifo_VC1E1[4:0]),
			      .empty_VC1E1	(empty_VC1E1));

	arbitraje_p probador (/*AUTOINST*/
			      // Outputs
			      .clk		(clk_16f),
			      .rst		(rst),
			      .rst_clk		(rst_clk),
			      .data_fifo_VC0E0	(data_fifo_VC0E0[4:0]),
			      .empty_VC0E0	(empty_VC0E0),
			      .data_fifo_VC1E0	(data_fifo_VC1E0[4:0]),
			      .empty_VC1E0	(empty_VC1E0),
			      .data_fifo_VC0E1	(data_fifo_VC0E1[4:0]),
			      .empty_VC0E1	(empty_VC0E1),
			      .data_fifo_VC1E1	(data_fifo_VC1E1[4:0]),
			      .empty_VC1E1	(empty_VC1E1),
			      // Inputs
			      .pop_req_VC0E0_c	(pop_req_VC0E0_c),
			      .pop_req_VC1E0_c	(pop_req_VC1E0_c),
			      .pop_req_VC0E1_c	(pop_req_VC0E1_c),
			      .pop_req_VC1E1_c	(pop_req_VC1E1_c),
			      .puerto_salida_0_c	(puerto_salida_0_c[4:0]),
			      .puerto_salida_1_c	(puerto_salida_1_c[4:0]),
			      .valid_puerto_salida_0_c(valid_puerto_salida_0_c),
			      .valid_puerto_salida_1_c(valid_puerto_salida_1_c),
			      .pop_req_VC0E0_e	(pop_req_VC0E0_e),
			      .pop_req_VC1E0_e	(pop_req_VC1E0_e),
			      .pop_req_VC0E1_e	(pop_req_VC0E1_e),
			      .pop_req_VC1E1_e	(pop_req_VC1E1_e),
			      .puerto_salida_0_e	(puerto_salida_0_e[4:0]),
			      .puerto_salida_1_e	(puerto_salida_1_e[4:0]),
			      .valid_puerto_salida_0_e(valid_puerto_salida_0_e),
			      .valid_puerto_salida_1_e(valid_puerto_salida_1_e));

	arbitraje_estructural estructural (/*AUTOINST*/
					   // Outputs
					   .pop_req_VC0E0	(pop_req_VC0E0_e),
					   .pop_req_VC0E1	(pop_req_VC0E1_e),
					   .pop_req_VC1E0	(pop_req_VC1E0_e),
					   .pop_req_VC1E1	(pop_req_VC1E1_e),
					   .puerto_salida_0	(puerto_salida_0_e[4:0]),
					   .puerto_salida_1	(puerto_salida_1_e[4:0]),
					   .valid_puerto_salida_0(valid_puerto_salida_0_e),
					   .valid_puerto_salida_1(valid_puerto_salida_1_e),
					   // Inputs
					   .clk			(clk_8f),
					   .clk_2f		(clk_16f),
					   .data_fifo_VC0E0	(data_fifo_VC0E0[4:0]),
					   .data_fifo_VC0E1	(data_fifo_VC0E1[4:0]),
					   .data_fifo_VC1E0	(data_fifo_VC1E0[4:0]),
					   .data_fifo_VC1E1	(data_fifo_VC1E1[4:0]),
					   .empty_VC0E0		(empty_VC0E0),
					   .empty_VC0E1		(empty_VC0E1),
					   .empty_VC1E0		(empty_VC1E0),
					   .empty_VC1E1		(empty_VC1E1),
					   .rst			(rst));

endmodule
