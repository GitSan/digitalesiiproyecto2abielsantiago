module memory_p (
	output reg [4:0] data_in,
	output reg [2:0] wr_addr, rd_addr,
	output reg wr, rd, clk, rst,
	input [4:0] data_out_cond, data_out_estr);

	//Reloj
	initial clk <= 0;
	always #1 clk <= ~clk;

	//Pruebas
	initial begin
		@(posedge clk);
		rst <= 1'b0;		//rst, desactivado
		wr <= 1'b0;
		rd <= 1'b0;
		wr_addr <= 0;
		rd_addr <= 0;
		data_in <= 5'b00000;
		@(posedge clk);		//escribo en el campo 0
		rst <= 1'b1;
		wr <= 1'b1;
		rd <= 1'b0;
		wr_addr <= 0;
		rd_addr <= 0;
		data_in <= 5'b10001;
		@(posedge clk);		//leo el campo 0
		rst <= 1'b1;
		wr <= 1'b0;
		rd <= 1'b1;
		wr_addr <= 0;
		rd_addr <= 0;
		data_in <= 5'b00000;
		@(posedge clk);		//escribo en el campo 1 y leo el campo 0
		rst <= 1'b1;
		wr <= 1'b1;
		rd <= 1'b1;
		wr_addr <= 1;
		rd_addr <= 0;
		data_in <= 5'b00110;
		@(posedge clk);		//escribo en el campo 1 y leo el campo 1
		rst <= 1'b1;
		wr <= 1'b1;
		rd <= 1'b1;
		wr_addr <= 1;
		rd_addr <= 1;
		data_in <= 5'b10101;
		@(posedge clk);		//leo el campo 2
		rst <= 1'b1;
		wr <= 1'b0;
		rd <= 1'b1;
		wr_addr <= 0;
		rd_addr <= 2;
		data_in <= 5'b00000;
		#20
		$finish;
	end

endmodule
