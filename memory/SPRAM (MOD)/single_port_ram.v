module single_port_ram
(
	input [4:0] data_in,
	input [2:0] wr_addr, rd_addr,
	input wr, rd, clk, rst,
	output reg [4:0] data_out
);

	// Declare the RAM variable
	reg [4:0] ram[39:0];	//ocho palabras de 5 bits c/u
	
	// Variable to hold the registered read address
	reg [2:0] wr_addr_reg, rd_addr_reg;
	
	always @ (posedge clk)
	begin
		if (rst) begin
		// Write
			if (wr)
				ram[wr_addr] <= data_in;
		
//			wr_addr_reg <= wr_addr;

		//Read
			if (rd)
				data_out <= ram[rd_addr];
		
		end
		
		// Continuous assignment implies read returns NEW data.
		// This is the natural behavior of the TriMatrix memory
		// blocks in Single Port mode.  
//		assign q = ram[addr_reg];

	end
	
endmodule
