`include "single_port_ram.v"
`include "memory_p.v"
`include "memory_estructural_auto_mapped.v"
`include "cmos_cells.v"
`timescale 1ns/100ps

module memory_tb;

	wire [4:0] data_in, data_out_cond, data_out_estr;
	wire [2:0] wr_addr, rd_addr;
	wire wr, rd, clk, rst;

	single_port_ram conductual (/*AUTOINST*/
				    // Outputs
				    .data_out		(data_out_cond[4:0]),
				    // Inputs
				    .data_in		(data_in[4:0]),
				    .wr_addr		(wr_addr[2:0]),
				    .rd_addr		(rd_addr[2:0]),
				    .wr			(wr),
				    .rd			(rd),
				    .clk		(clk),
				    .rst		(rst));

	memory_p probador (/*AUTOINST*/
			   // Outputs
			   .data_in		(data_in[4:0]),
			   .wr_addr		(wr_addr[2:0]),
			   .rd_addr		(rd_addr[2:0]),
			   .wr			(wr),
			   .rd			(rd),
			   .clk			(clk),
			   .rst			(rst),
			   // Inputs
			   .data_out_cond		(data_out_cond[4:0]),
			   .data_out_estr		(data_out_estr[4:0]));

	memory_estructural_auto_mapped estructural (/*AUTOINST*/
				    // Outputs
				    .data_out		(data_out_estr[4:0]),
				    // Inputs
				    .data_in		(data_in[4:0]),
				    .wr_addr		(wr_addr[2:0]),
				    .rd_addr		(rd_addr[2:0]),
				    .wr			(wr),
				    .rd			(rd),
				    .clk		(clk),
				    .rst		(rst));

	initial begin
	$dumpfile("memory.vcd");
		$dumpvars;
	end

endmodule
