//`include "dual_ram.v"
//`include "mem_p.v"
`include "mem_estructural_auto_mapped.v"
`include "cmos_cells.v"

module mem_tb;

	wire clk, reset, wr, rd;
	wire [4:0] data_in, data_out_cond, data_out_estr;
	wire [2:0] wr_addr, rd_addr;
integer idx;
	dual_ram conductual (/*AUTOINST*/
			     // Outputs
			     .data_out		(data_out_cond[4:0]),
			     // Inputs
			     .clk		(clk),
			     .reset		(reset),
			     .wr		(wr),
			     .rd		(rd),
			     .data_in		(data_in[4:0]),
			     .wr_addr		(wr_addr[2:0]),
			     .rd_addr		(rd_addr[2:0]));

	mem_p probador (/*AUTOINST*/
			// Outputs
			.clk		(clk),
			.reset		(reset),
			.wr		(wr),
			.rd		(rd),
			.data_in	(data_in[4:0]),
			.wr_addr	(wr_addr[2:0]),
			.rd_addr	(rd_addr[2:0]),
			// Inputs
			.data_out_cond	(data_out_cond[4:0]),
			.data_out_estr	(data_out_estr[4:0]));

	mem_estructural_auto_mapped estructural (/*AUTOINST*/
			       // Outputs
			       .data_out	(data_out_estr[4:0]),
			       // Inputs
			       .clk		(clk),
			       .data_in		(data_in[4:0]),
			       .rd		(rd),
			       .rd_addr		(rd_addr[2:0]),
			       .reset		(reset),
			       .wr		(wr),
			       .wr_addr		(wr_addr[2:0]));

	initial begin
	$dumpfile("mem.vcd");
		$dumpvars;
		for (idx = 0; idx < (8); idx = idx + 1) begin
             $dumpvars(1,conductual.mem[idx]);
// by  2018-11-14 |              $dumpvars(1,estructural.mem[idx]);
         end

	end

endmodule
