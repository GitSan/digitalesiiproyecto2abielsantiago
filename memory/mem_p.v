module mem_p (
	output reg clk, reset, wr, rd,
	output reg [4:0] data_in,
	output reg [2:0] wr_addr, rd_addr,
	input [4:0] data_out_cond,
	input [4:0] data_out_estr);


	//Reloj
	initial clk <= 0;
	always #5 clk <= ~clk;

    //Declaración de variables internas
    reg compare;
    reg [4:0] conductual, estructural;

	//Pruebas
	initial begin
		@(posedge clk);
		reset <= 1'b1;
		wr <= 1'b0;
		rd <= 1'b0;
		data_in <= 5'b00000;
		wr_addr <= 3'b000;
		rd_addr <= 3'b000;
		@(posedge clk);
		reset <= 1'b0;
		wr <= 1'b1;
		rd <= 1'b0;
		data_in <= 5'h0A;
		wr_addr <= 3'b000;
		rd_addr <= 3'b000;
		@(posedge clk);
		reset <= 1'b0;
// by  2018-11-14 | 		wr <= 1'b1;
		rd <= 1'b0;
		data_in <= 5'h0B;
		wr_addr <= 3'b001;
		rd_addr <= 3'b010;
		@(posedge clk);
		reset <= 1'b0;
// by  2018-11-14 | 		wr <= 1'b0;
		rd <= 1'b0;
		data_in <= 5'hC;
		wr_addr <= 3'b010;
		rd_addr <= 3'b000;
		@(posedge clk);
		reset <= 1'b0;
// by  2018-11-14 | 		wr <= 1'b0;
		rd <= 1'b0;
		data_in <= 5'h0D;
		wr_addr <= 3'b011;
		rd_addr <= 3'b000;
		@(posedge clk);
		reset <= 1'b0;
// by  2018-11-14 | 		wr <= 1'b0;
		rd <= 1'b0;
		data_in <= 5'h0E;
		wr_addr <= 3'b100;
		rd_addr <= 3'b000;
		@(posedge clk);
		reset <= 1'b0;
// by  2018-11-14 | 		wr <= 1'b0;
		rd <= 1'b0;
		data_in <= 5'h08;
		wr_addr <= 3'b101;
		rd_addr <= 3'b000;
		@(posedge clk);
		reset <= 1'b0;
// by  2018-11-14 | 		wr <= 1'b0;
		rd <= 1'b0;
		data_in <= 5'h07;
		wr_addr <= 3'b110;
		rd_addr <= 3'b000;
		@(posedge clk);
		reset <= 1'b0;
// by  2018-11-14 | 		wr <= 1'b0;
		rd <= 1'b0;
		data_in <= 5'h06;
		wr_addr <= 3'b111;
		rd_addr <= 3'b000;
		@(posedge clk);
		reset <= 1'b0;
		wr <= 1'b1;
		rd <= 1'b0;
		data_in <= 5'b01111;
		wr_addr <= 3'b000;
		rd_addr <= 3'b000;
		@(posedge clk);
		reset <= 1'b0;
		wr <= 1'b0;
		rd <= 1'b1;
		data_in <= 5'b11110;
		wr_addr <= 3'b000;
		rd_addr <= 3'b000;
		@(posedge clk);
		reset <= 1'b0;
		wr <= 1'b1;
		rd <= 1'b0;
		data_in <= 5'b01100;
		wr_addr <= 3'b110;
		rd_addr <= 3'b000;
		@(posedge clk);
		reset <= 1'b0;
		wr <= 1'b0;
		rd <= 1'b1;
		data_in <= 5'b01100;
		wr_addr <= 3'b000;
		rd_addr <= 3'b011;
		@(posedge clk);
		reset <= 1'b0;
		wr <= 1'b1;
		rd <= 1'b0;
		data_in <= 5'b10111;
		wr_addr <= 3'b010;
		rd_addr <= 3'b011;
		@(posedge clk);
		reset <= 1'b0;
		wr <= 1'b1;
		rd <= 1'b1;
		data_in <= 5'b01000;
		wr_addr <= 3'b101;
		rd_addr <= 3'b010;
		@(posedge clk);
		reset <= 1'b0;
		wr <= 1'b0;
		rd <= 1'b1;
		data_in <= 5'b11000;
		wr_addr <= 3'b000;
		rd_addr <= 3'b010;
		@(posedge clk);
		reset <= 1'b0;
		wr <= 1'b0;
		rd <= 1'b0;
		data_in <= 5'b00000;
		wr_addr <= 3'b000;
		rd_addr <= 3'b000;
		@(posedge clk);
		reset <= 1'b0;
		wr <= 1'b1;
		rd <= 1'b0;
		data_in <= 5'b01111;
		wr_addr <= 3'b000;
		rd_addr <= 3'b000;
		@(posedge clk);
		reset <= 1'b0;
		wr <= 1'b0;
		rd <= 1'b1;
		data_in <= 5'b11110;
		wr_addr <= 3'b000;
		rd_addr <= 3'b000;
		@(posedge clk);
		reset <= 1'b0;
		wr <= 1'b1;
		rd <= 1'b0;
		data_in <= 5'b01100;
		wr_addr <= 3'b110;
		rd_addr <= 3'b000;
		@(posedge clk);
		reset <= 1'b0;
		wr <= 1'b0;
		rd <= 1'b1;
		data_in <= 5'b01100;
		wr_addr <= 3'b000;
		rd_addr <= 3'b011;
		@(posedge clk);
		reset <= 1'b0;
		wr <= 1'b1;
		rd <= 1'b0;
		data_in <= 5'b10111;
		wr_addr <= 3'b010;
		rd_addr <= 3'b011;
		@(posedge clk);
		#20
		$finish;
	end

	//Asignación de salidas de los multiplexores a registros internos de probador.v para comparación
    always @(posedge clk)
    begin
        conductual <= data_out_cond;
        estructural <= data_out_estr;
    end
    //Lógica de comparación
    always @(*)
    begin
        compare = 0;
        if(conductual != estructural)
        begin
            compare = 1;
        end
	end

endmodule
