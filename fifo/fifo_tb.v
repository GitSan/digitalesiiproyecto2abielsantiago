`include "fifo_estructural.v"
`include "cmos_cells.v"
`include "fifo_p.v"
`include "fifo.v"
`timescale 1ns / 100ps
module fifo_tb ();
        
    wire clk, reset, push, pop;
    wire [3:0] data_in;
	wire fifo_full_c, fifo_empty_c, fifo_almost_full_pause_c, fifo_almost_empty_continue_c, fifo_error_c, dest_out_c;
	wire [3:0] data_out_c;
	wire fifo_full_e, fifo_empty_e, fifo_almost_full_pause_e, fifo_almost_empty_continue_e, fifo_error_e, dest_out_e;
	wire [3:0] data_out_e;
	
	integer idx;

    fifo_p probador (/*autoinst*/
		     // Outputs
		     .data_in		(data_in[3:0]),
		     .dest_in		(dest_in),
		     .clk		(clk),
		     .reset		(reset),
		     .push		(push),
		     .pop		(pop),
		     // Inputs
		     .fifo_full_c	(fifo_full_c),
		     .fifo_empty_c	(fifo_empty_c),
		     .fifo_almost_full_pause_c(fifo_almost_full_pause_c),
		     .fifo_almost_empty_continue_c(fifo_almost_empty_continue_c),
		     .fifo_error_c	(fifo_error_c),
		     .data_out_c	(data_out_c[3:0]),
		     .dest_out_c	(dest_out_c),
		     .fifo_full_e	(fifo_full_e),
		     .fifo_empty_e	(fifo_empty_e),
		     .fifo_almost_full_pause_e(fifo_almost_full_pause_e),
		     .fifo_almost_empty_continue_e(fifo_almost_empty_continue_e),
		     .fifo_error_e	(fifo_error_e),
		     .data_out_e	(data_out_e[3:0]),
		     .dest_out_e	(dest_out_e));

    fifo conductual (/*autoinst*/
		     // Outputs
		     .fifo_full		(fifo_full_c),
		     .fifo_empty	(fifo_empty_c),
		     .fifo_almost_full_pause(fifo_almost_full_pause_c),
		     .fifo_almost_empty_continue(fifo_almost_empty_continue_c),
		     .fifo_error	(fifo_error_c),
		     .data_out		(data_out_c[3:0]),
		     .dest_out		(dest_out_c),
		     // Inputs
		     .data_in		(data_in[3:0]),
		     .dest_in		(dest_in),
		     .clk		(clk),
		     .reset		(reset),
		     .push		(push),
		     .pop		(pop));

    fifo_estructural estructual (/*autoinst*/
		     // Outputs
		     .fifo_full		(fifo_full_e),
		     .fifo_empty	(fifo_empty_e),
		     .fifo_almost_full_pause(fifo_almost_full_pause_e),
		     .fifo_almost_empty_continue(fifo_almost_empty_continue_e),
		     .fifo_error	(fifo_error_e),
		     .data_out		(data_out_e[3:0]),
		     .dest_out		(dest_out_e),
		     // Inputs
		     .data_in		(data_in[3:0]),
		     .dest_in		(dest_in),
		     .clk		(clk),
		     .reset		(reset),
		     .push		(push),
		     .pop		(pop));

	initial begin
		$dumpfile("fifo.vcd");
        $dumpvars;
        for (idx = 0; idx < (8); idx = idx + 1) begin
			$dumpvars(1,conductual.mem.mem[idx]);
// by  2018-11-18 | 			$dumpvars(1,estructural.mem.mem[idx]);
        end
    end

endmodule
