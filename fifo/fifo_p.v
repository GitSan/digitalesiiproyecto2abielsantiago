module fifo_p (
	output reg [3:0] data_in,			//dato entrada de 4 bits	
	output reg dest_in,					//quinto bit del dato entrada, entrada destino
	output reg clk, reset, push, pop,			//reloj, reset, write, read
	input fifo_full_c, fifo_empty_c,	//señales de salida de lleno y vacío
	input fifo_almost_full_pause_c, fifo_almost_empty_continue_c, //señales de salida de pause y continue
	input fifo_error_c, //Si se hace escritura y no lectura cuando fifo está lleno.
	input [3:0] data_out_c,		//dato salida
	input dest_out_c,			//quinto bit del dato salida, salida destino
	input fifo_full_e, fifo_empty_e,	//señales de salida de lleno y vacío
	input fifo_almost_full_pause_e, fifo_almost_empty_continue_e, //señales de salida de pause y continue
	input fifo_error_e, //Si se hace escritura y no lectura cuando fifo está lleno.
	input [3:0] data_out_e,		//dato salida
	input dest_out_e);			//quinto bit del dato salida, salida destino

    //Reloj
    initial clk <= 0;
    always #1 clk <= ~clk;

    //Declaración de variables internas
    reg compare;
    reg [9:0] conductual, estructural;

   //Pruebas
    initial begin
        @(posedge clk);
        reset <= 1'b1;
		data_in <= 4'hA;
		dest_in <= 1;
		push<= 0;
		pop<=0;
        @(posedge clk);@(posedge clk);
        reset <= 1'b0;
        @(posedge clk);
		push<= 1;
		data_in <= 4'hA;
        @(posedge clk);
		pop<=1;
		data_in <= 4'hf;
        @(posedge clk);
		data_in <= 4'he;
        @(posedge clk);
		data_in <= 4'hd;
        @(posedge clk);
		data_in <= 4'hc;
        @(posedge clk);
		data_in <= 4'hb;
        @(posedge clk);
		data_in <= 4'hA;
        @(posedge clk);
		data_in <= 4'h9;
        @(posedge clk);
		data_in <= 4'h8;
        @(posedge clk);
		data_in <= 4'h7;
        @(posedge clk);
		data_in <= 4'h6;
        @(posedge clk);@(posedge clk);
		pop<=0;
        @(posedge clk);@(posedge clk);
		push<= 1;
        #20 @(posedge clk);@(posedge clk);
		push<= 0;
        @(posedge clk);@(posedge clk);
		pop<=1;
		data_in <= 4'hC;
        @(posedge clk);@(posedge clk);
		pop<=0;
        @(posedge clk);@(posedge clk);
		pop<= 1;
        @(posedge clk);@(posedge clk);
		push<=1;
        @(posedge clk);@(posedge clk);
		pop<=0;
        @(posedge clk);@(posedge clk);
		push<=0;
        #20 @(posedge clk);@(posedge clk);
		pop<= 1;
        @(posedge clk);@(posedge clk);
        #20 $finish;
    end

    //Asignación de salidas de los multiplexores a registros internos de probador.v para comparación
    always @(posedge clk)
    begin
        conductual <= {fifo_error_c,fifo_full_c,fifo_empty_c,fifo_almost_full_pause_c,fifo_almost_empty_continue_c,dest_out_c,data_out_c};
        estructural <= {fifo_error_e,fifo_full_e,fifo_empty_e,fifo_almost_full_pause_e,fifo_almost_empty_continue_e,dest_out_e,data_out_e};
    end
    //Lógica de comparación
    always @(*)
    begin
        compare = 0;
        if(conductual != estructural)
        begin
            compare = 1;
        end
    end

endmodule                       
