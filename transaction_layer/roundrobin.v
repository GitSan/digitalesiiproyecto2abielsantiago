// +FHDR------------------------------------------------------------
//                 Copyright (c) 2018 .
//                       ALL RIGHTS RESERVED
// -----------------------------------------------------------------
// Filename      : roundrobin.v
// Author        : 
// Created On    : 2018-11-14 17:28
// Last Modified : 
// -----------------------------------------------------------------
// Description:
//
//
// -FHDR------------------------------------------------------------

`timescale 1ns/100ps

module roundrobin(
    input clk,
	input request0,
	input request1,
	output valid,
	output id
);

	wire both_request, selector; 

	assign valid = ( request0||request1)?1:0;
	assign both_request = (request0 && request1)?1:0;
	assign selector = clk;
    assign id = (both_request)?((selector)?1:0):((request1)?1:0);

endmodule
