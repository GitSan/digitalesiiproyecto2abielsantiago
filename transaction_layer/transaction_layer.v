// +FHDR------------------------------------------------------------
//                 Copyright (c) 2018 .
//                       ALL RIGHTS RESERVED
// -----------------------------------------------------------------
// Filename      : transaction_layer.v
// Author        : 
// Created On    : 2018-11-18 18:54
// Last Modified : 
// -----------------------------------------------------------------
// Description:
//
//
// -FHDR------------------------------------------------------------

`include "fifo.v"
`include "arbitraje.v"
`include "fsm.v"
`timescale 1ns/100ps

module transaction_layer(
	//INPUTS
	input clk,
	input clk_2f,
	input rst,
	//push
	input push_VC0E0,
	input push_VC1E0,
	input push_VC0E1,
	input push_VC1E1,
	//FSM de control
	input [5:0] umbral,/*MSB:110 = almost full, LSB:100 = almost empty*/
	input init,
	//vc_ids
	input vc_id0,
	input vc_id1,
	//dests
	input dest0,
	input dest1,
	//datas
	input [3:0] data_in0,
	input [3:0] data_in1,
	input valid_data_in0,
	input valid_data_in1,
	//OUTPUTS
	//E0
	output pause0_vc0,
	output continue0_vc0,
	output pause0_vc1,
	output continue0_vc1,
	//E1
	output pause1_vc0,
	output continue1_vc0,
	output pause1_vc1,
	output continue1_vc1,
	//FSM
	output error,
	output active,
	output idle,
	//arbitraje
	output [4:0] data_out0,
	output [4:0] data_out1,
	output valid_data_out0,
	output valid_data_out1
);

	//wires demux entrada
	wire [4:0] demuxE0_out_0, demuxE0_out_1, demuxE1_out_0, demuxE1_out_1;
	wire valid_demuxE0_out_0, valid_demuxE0_out_1, valid_demuxE1_out_0, valid_demuxE1_out_1;

    //wires fifos
	wire push_VC0E00, push_VC1E00, push_VC0E11, push_VC1E11;
// by  2018-11-25 |     wire [3:0] fifoE0_in_0, fifoE0_in_1, fifoE1_in_0, fifoE1_in_1;
	wire [3:0] data_out_VC0E0, data_out_VC1E0, data_out_VC0E1, data_out_VC1E1;
//    wire push_VC0E0, push_VC1E0,push_VC0E1,push_VC1E1;
	wire pop_VC0E0,pop_VC1E0,pop_VC0E1,pop_VC1E1;
	wire fifo_full_VC0E0, fifo_full_VC1E0, fifo_full_VC0E1, fifo_full_VC1E1;
	wire fifo_empty_VC0E0, fifo_empty_VC1E0, fifo_empty_VC0E1, fifo_empty_VC1E1;
	wire fifo_almost_full_pause_VC0E0, fifo_almost_full_pause_VC1E0, fifo_almost_full_pause_VC0E1, fifo_almost_full_pause_VC1E1;
	wire fifo_almost_empty_continue_VC0E0, fifo_almost_empty_continue_VC1E0, fifo_almost_empty_continue_VC0E1, fifo_almost_empty_continue_VC1E1;
	wire fifo_error_VC0E0,fifo_error_VC1E0,fifo_error_VC0E1,fifo_error_VC1E1;
	wire dest_out_VC0E0, dest_out_VC1E0, dest_out_VC0E1, dest_out_VC1E1;

    //wires FSM de control
	wire [4:0] fifo_empty, fifo_error;
	wire [2:0] umbral_almost_empty, umbral_almost_full;
	wire [5:0] umbrales;


    //demuxes entrada
	demux_5bits demuxE0 (vc_id0, clk_2f, rst, demuxE0_out_0[4:0], valid_demuxE0_out_0, valid_demuxE0_out_1, demuxE0_out_1[4:0], valid_data_in0, {dest0,data_in0[3:0]});
	demux_5bits demuxE1 (vc_id1, clk_2f, rst, demuxE1_out_0[4:0], valid_demuxE1_out_0, valid_demuxE1_out_1, demuxE1_out_1[4:0], valid_data_in1, {dest1,data_in1[3:0]});


	assign push_VC0E00 = valid_demuxE0_out_0; 
	assign push_VC1E00 =valid_demuxE0_out_1; 
	assign push_VC0E11 = valid_demuxE1_out_0; 
	assign push_VC1E11 =valid_demuxE1_out_1; 


    //flops para sincronizar push reqs. Necesita 1 flop.
	reg flop0_push_VC0E0, flop0_push_VC1E0, flop0_push_VC0E1, flop0_push_VC1E1; //, flop4_push_VC0E0; 
	always @(posedge clk_2f)
	begin
        flop0_push_VC0E0 <= push_VC0E00;
        flop0_push_VC1E0 <= push_VC1E00;
        flop0_push_VC0E1 <= push_VC0E11;
        flop0_push_VC1E1 <= push_VC1E11;
	end
    //flops para sincronizar pop reqs. Necesita 2 flops.
// by  2018-12-02 | 	reg flop0_pop_VC0E0, flop0_pop_VC1E0, flop0_pop_VC0E1, flop0_pop_VC1E1;
// by  2018-12-02 | 	reg flop1_pop_VC0E0, flop1_pop_VC1E0, flop1_pop_VC0E1, flop1_pop_VC1E1;
// by  2018-12-02 | 	always @(posedge clk_2f)
// by  2018-12-02 | 	begin
// by  2018-12-02 |         flop0_pop_VC0E0 <= pop_req_VC0E0;
// by  2018-12-02 |         flop0_pop_VC1E0 <= pop_req_VC1E0;
// by  2018-12-02 |         flop0_pop_VC0E1 <= pop_req_VC0E1;
// by  2018-12-02 |         flop0_pop_VC1E1 <= pop_req_VC1E1;
// by  2018-12-02 |         flop1_pop_VC0E0 <= flop0_pop_VC0E0;
// by  2018-12-02 |         flop1_pop_VC1E0 <= flop0_pop_VC1E0;
// by  2018-12-02 |         flop1_pop_VC0E1 <= flop0_pop_VC0E1;
// by  2018-12-02 |         flop1_pop_VC1E1 <= flop0_pop_VC1E1;
// by  2018-12-02 | 	end

    //FIFOs
	fifo fifoVC0E0 (demuxE0_out_0[3:0], demuxE0_out_0[4], clk_2f, !rst, flop0_push_VC0E0, pop_req_VC0E0, umbral_almost_empty[2:0], umbral_almost_full[2:0], fifo_full_VC0E0, fifo_empty_VC0E0, fifo_almost_full_pause_VC0E0, fifo_almost_empty_continue_VC0E0, fifo_error_VC0E0, data_out_VC0E0[3:0], dest_out_VC0E0);
	fifo fifoVC1E0 (demuxE0_out_1[3:0], demuxE0_out_1[4], clk_2f, !rst, flop0_push_VC1E0, pop_req_VC1E0, umbral_almost_empty[2:0], umbral_almost_full[2:0], fifo_full_VC1E0, fifo_empty_VC1E0, fifo_almost_full_pause_VC1E0, fifo_almost_empty_continue_VC1E0, fifo_error_VC1E0, data_out_VC1E0[3:0], dest_out_VC1E0);
	fifo fifoVC0E1 (demuxE1_out_0[3:0], demuxE1_out_0[4], clk_2f, !rst, flop0_push_VC0E1, pop_req_VC0E1, umbral_almost_empty[2:0], umbral_almost_full[2:0], fifo_full_VC0E1, fifo_empty_VC0E1, fifo_almost_full_pause_VC0E1, fifo_almost_empty_continue_VC0E1, fifo_errorVC0E1, data_out_VC0E1[3:0], dest_out_VC0E1);
	fifo fifoVC1E1 (demuxE1_out_1[3:0], demuxE1_out_1[4], clk_2f, !rst, flop0_push_VC1E1, pop_req_VC1E1, umbral_almost_empty[2:0], umbral_almost_full[2:0], fifo_full_VC1E1, fifo_empty_VC1E1, fifo_almost_full_pause_VC1E1, fifo_almost_empty_continue_VC1E1, fifo_errorVC1E1, data_out_VC1E1[3:0], dest_out_VC1E1);

    assign pause0_vc0 = fifo_almost_full_pause_VC0E0;
	assign continue0_vc0 = fifo_almost_empty_continue_VC0E0;
    assign pause0_vc1 = fifo_almost_full_pause_VC1E0;
	assign continue0_vc1 = fifo_almost_empty_continue_VC1E0;
    assign pause1_vc0 = fifo_almost_full_pause_VC0E1;
	assign continue1_vc0 = fifo_almost_empty_continue_VC0E1;
    assign pause1_vc1 = fifo_almost_full_pause_VC1E1;
	assign continue1_vc1 = fifo_almost_empty_continue_VC1E1;

    //arbitraje
	arbitraje arbitro (clk, clk_2f, rst,{dest_out_VC0E0,data_out_VC0E0[3:0]}, fifo_empty_VC0E0, pop_req_VC0E0, {dest_out_VC1E0,data_out_VC1E0[3:0]}, fifo_empty_VC1E0, pop_req_VC1E0, {dest_out_VC0E1,data_out_VC0E1[3:0]}, fifo_empty_VC0E1, pop_req_VC0E1,{dest_out_VC1E1,data_out_VC1E1[3:0]}, fifo_empty_VC1E1, pop_req_VC1E1, data_out0[4:0], data_out1[4:0], valid_data_out0, valid_data_out1);

	//FSM de control
	assign fifo_empty[3:0] = {fifo_empty_VC0E0, fifo_empty_VC1E0, fifo_empty_VC0E1, fifo_empty_VC1E1};
	assign fifo_error[3:0] = {fifo_error_VC0E0, fifo_error_VC1E0, fifo_errorVC0E1, fifo_errorVC1E1};
	assign umbral_almost_empty[2:0] = umbrales[2:0];
	assign umbral_almost_full[2:0] = umbrales[5:3];
	fsm control (clk, !rst, init, fifo_empty[3:0], fifo_error[3:0], umbral[5:0], umbrales[5:0], idle, active, error);

endmodule
