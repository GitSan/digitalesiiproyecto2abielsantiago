//-----------------------------------------------------------
// Máquina de estados finitos con 3 always blocks.
//Onehot FSM Coding Style
//----------------------------------------------------------
module fsm (
	input clk,
	input reset,
	input init,
	input [3:0] fifo_empty, /*4'b[VC0E0,VC1E0,VC0E1,VC1E1]*/
	input [3:0] fifo_error, /*4'b[VC0E0,VC1E0,VC0E1,VC1E1]*/
	input [5:0] umbral, /*MSB:110 = almost full, LSB:011 = almost empty*/
	output reg [5:0] umbrales, /*MSB:110 = almost full, LSB:011 = almost empty*/ 
	output reg idle,
	output reg active,
	output reg error);

	parameter [4:0] RESET  = 0,
					INIT   = 1,
					IDLE   = 2,
					ACTIVE = 3,
					ERROR  = 4;

	reg [4:0] state, next_state;

	always @(posedge clk)
	begin
		if (reset) 
		begin
			state		 <= 5'b0;
			state[RESET] <= 1'b1;
		end
		else 
		begin
			state <= next_state;
		end
	end

	always @(*) 
	begin
		next_state = 5'b0;
		idle = 0;
		error= 0;
		active = 0;
		case (1'b1)	//ambit synthesis case = full, parallel
			state[RESET]  :		begin
									if (reset)                      			next_state[RESET]  = 1'b1;
									else										next_state[INIT]   = 1'b1;
								end

			state[INIT]   :		begin
									if (reset)									next_state[RESET]  = 1'b1;
									else
									begin
										if (init)    							next_state[INIT]   = 1'b1;
										else									next_state[IDLE]   = 1'b1;
									end
								end

			state[IDLE]   :		begin
									idle = 1'b1;
									if (reset)								next_state[RESET]  = 1'b1;
									else
									begin
										if (init) 							next_state[INIT]   = 1'b1;
										else
										begin
											if (&fifo_empty[3:0])        	next_state[IDLE]   = 1'b1;
											else							next_state[ACTIVE] = 1'b1;
										end
									end
								end

			state[ACTIVE] :		begin
			                        active = 1;
									if (reset)								next_state[RESET]  = 1'b1;
									else
									begin
										if (init) 							next_state[INIT]   = 1'b1;
										else
										begin
											if (&fifo_empty[3:0])        	next_state[IDLE]   = 1'b1;
											else
											begin
												if (|fifo_error[3:0])		next_state[ERROR]  = 1'b1;
												else				        next_state[ACTIVE] = 1'b1;
										    end
										end
									end
								end

			state[ERROR]  :		begin
									error = 1'b1;
									if (reset)								next_state[RESET]  = 1'b1;
									else							        next_state[ERROR]  = 1'b1;
								end
		endcase

	end

    always @(posedge clk)
    begin
        if (reset) umbrales <= 0;
	    else 
	    begin
	        umbrales[5:0] <= umbrales[5:0];
	        case(1'b1)
		    next_state[RESET], next_state[IDLE], next_state[ACTIVE], next_state[ERROR]: ;//default outputs
		    next_state[INIT]:   begin
									if(init)	umbrales[5:0] <= umbral[5:0];
								end
		    endcase
		end
    end

endmodule
