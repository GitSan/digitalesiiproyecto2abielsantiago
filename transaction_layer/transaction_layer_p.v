// +FHDR------------------------------------------------------------
//                 Copyright (c) 2018 .
//                       ALL RIGHTS RESERVED
// -----------------------------------------------------------------
// Filename      : transaction_layer_p.v
// Author        : 
// Created On    : 2018-11-18 18:55
// Last Modified : 
// -----------------------------------------------------------------
// Description:
//
//
// -FHDR------------------------------------------------------------

`timescale 1ns/100ps

module transaction_layer_p(
	//output regs
	output reg clk,
	output reg rst,
	output reg rst_clk,
	//push
	output reg push_VC0E0,
	output reg push_VC1E0,
	output reg push_VC0E1,
	output reg push_VC1E1,
	//FSM de control
	output reg [5:0] umbral,/*MSB:110 = almost full, LSB:011 = almost empty*/
	output reg init,
	//vc_ids
	output reg vc_id0,
	output reg vc_id1,
	//dests
	output reg dest0,
	output reg dest1,
	//datas
	output reg [3:0] data_in0,
	output reg [3:0] data_in1,
	output reg valid_data_in0,
	output reg valid_data_in1,
	//INPUTS
	//E0
	input pause0_vc0_c,
	input continue0_vc0_c,
	input pause0_vc1_c,
	input continue0_vc1_c,
	//E1
	input pause1_vc0_c,
	input continue1_vc0_c,
	input pause1_vc1_c,
	input continue1_vc1_c,
	//FSM
	input error_c,
	input active_c,
	input idle_c,
	//arbitraje
	input [4:0] data_out0_c,
	input [4:0] data_out1_c,
	input valid_data_out0_c,
	input valid_data_out1_c,
	//E0
	input pause0_vc0_e,
	input continue0_vc0_e,
	input pause0_vc1_e,
	input continue0_vc1_e,
	//E1
	input pause1_vc0_e,
	input continue1_vc0_e,
	input pause1_vc1_e,
	input continue1_vc1_e,
	//FSM
	input error_e,
	input active_e,
	input idle_e,
	//arbitraje
	input [4:0] data_out0_e,
	input [4:0] data_out1_e,
	input valid_data_out0_e,
	input valid_data_out1_e
);

	//Reloj
	initial clk <= 0;
	always #1 clk <= ~clk;

    //Declaración de variables internas
    reg compare;
    reg [9:0] conductual, estructural;

    initial begin
        $dumpfile("transaction_layer.vcd");
        $dumpvars;
		rst <= 0;
		rst_clk <= 0;
		umbral[5:0]<=0;
		init<=0;
		vc_id0<=0;
		vc_id1<=0;
		dest0<=0;
		dest1<=0;
		data_in0[3:0]<=4'hB;
		data_in1[3:0]<=4'h7;
		valid_data_in0<=0;
		valid_data_in1<=0;
		push_VC0E0 <= 0;
		push_VC1E0 <= 0;
		push_VC0E1 <= 0;
		push_VC1E1 <= 0;
		@(posedge clk);
		rst_clk <= 1;
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		rst <= 1;
		umbral[5:0]<=6'b110011;
		init<=1;
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		init<=0;
		vc_id0<=1;
		vc_id1<=1;
		dest0<=1;
		dest1<=0;
		data_in0[3:0]<=4'h6;
		data_in1[3:0]<=4'h1;
		valid_data_in0<=0;
		valid_data_in1<=1;
		push_VC0E1 <= 1;
		@(posedge clk);
		@(posedge clk);
		vc_id0<=1;
		vc_id1<=1;
		dest0<=0;
		dest1<=1;
		data_in0[3:0]<=4'hA;
		data_in1[3:0]<=4'h5;
		valid_data_in0<=0;
		valid_data_in1<=1;
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		vc_id0<=1;
		vc_id1<=1;
		dest0<=1;
		dest1<=1;
		data_in0[3:0]<=4'h1;
		data_in1[3:0]<=4'h8;
		valid_data_in0<=1;
		valid_data_in1<=1;
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		push_VC0E1 <= 0;
		vc_id0<=1;
		vc_id1<=0;
		dest0<=0;
		dest1<=1;
		data_in0[3:0]<=4'h2;
		data_in1[3:0]<=4'h7;
		valid_data_in0<=0;
		valid_data_in1<=0;
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		vc_id0<=0;
		vc_id1<=0;
		dest0<=0;
		dest1<=0;
		data_in0[3:0]<=4'h9;
		data_in1[3:0]<=4'h2;
		valid_data_in0<=1;
		valid_data_in1<=0;
		@(posedge clk);
		@(posedge clk);
		vc_id0<=1;
		vc_id1<=0;
		dest0<=0;
		dest1<=1;
		data_in0[3:0]<=4'h9;
		data_in1[3:0]<=4'h4;
		valid_data_in0<=1;
		valid_data_in1<=0;
		@(posedge clk);
		@(posedge clk);
		vc_id0<=1;
		vc_id1<=1;
		dest0<=1;
		dest1<=1;
		data_in0[3:0]<=4'hC;
		data_in1[3:0]<=4'h8;
		valid_data_in0<=0;
		valid_data_in1<=0;
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
        push_VC0E0 <= 1;
		@(posedge clk);			//pruebas min
		vc_id0<=0;
		vc_id1<=0;
		dest0<=0;
		dest1<=0;
		data_in0[3:0]<=4'hC;
		data_in1[3:0]<=4'h8;
		valid_data_in0<=0;
		valid_data_in1<=0;
		@(posedge clk);			//VC0E0
        push_VC0E0 <= 0;
		vc_id0<=0;
		vc_id1<=0;
		dest0<=0;
		dest1<=0;
		data_in0[3:0]<=4'hC;
		data_in1[3:0]<=4'h8;
		valid_data_in0<=1;
		valid_data_in1<=0;
		@(posedge clk);			//VC0E0 y VC0E1
		vc_id0<=0;
		vc_id1<=0;
		dest0<=0;
		dest1<=0;
		data_in0[3:0]<=4'hC;
		data_in1[3:0]<=4'h8;
		valid_data_in0<=1;
		valid_data_in1<=1;
		@(posedge clk);			//VC1E1
		vc_id0<=0;
		vc_id1<=1;
		dest0<=1;
		dest1<=0;
		data_in0[3:0]<=4'hC;
		data_in1[3:0]<=4'h8;
		valid_data_in0<=0;
		valid_data_in1<=1;
		@(posedge clk);			//VC0E1 y VC1E1
		vc_id0<=1;
		vc_id1<=1;
		dest0<=1;
		dest1<=0;
		data_in0[3:0]<=4'hC;
		data_in1[3:0]<=4'h8;
		valid_data_in0<=1;
		valid_data_in1<=1;
		@(posedge clk);				//VC0E1 y VC1E0
		vc_id0<=0;
		vc_id1<=1;
		dest0<=1;
		dest1<=0;
		data_in0[3:0]<=4'hC;
		data_in1[3:0]<=4'h8;
		valid_data_in0<=1;
		valid_data_in1<=1;
		@(posedge clk);
		vc_id0<=0;
		vc_id1<=0;
		dest0<=0;
		dest1<=0;
		data_in0[3:0]<=4'hC;
		data_in1[3:0]<=4'h8;
		valid_data_in0<=0;
		valid_data_in1<=0;
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		push_VC0E0 <= 1;
		@(posedge clk);
		@(posedge clk);
		push_VC0E0 <= 0;
	    push_VC0E1 <=1;	
		@(posedge clk);
		@(posedge clk);
		push_VC0E1 <= 0;

		@(posedge clk);
		#60 $finish;
	end

	//Asignación de salidas de los multiplexores a registros internos de probador para comparación
    always @(posedge clk)
    begin
        conductual[9:0] <= {error_c, idle_c, active_c, valid_data_out1_c, valid_data_out0_c, data_out1_c, data_out0_c}; 
        estructural[9:0] <= {error_e, idle_e, active_e, valid_data_out1_e, valid_data_out0_e, data_out1_e, data_out0_e}; 
    end
    //Lógica de comparación
    always @(*)
    begin
        compare = 0;
        if(conductual != estructural)
        begin
            compare = 1;
        end
    end


endmodule
