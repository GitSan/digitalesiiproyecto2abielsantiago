module dual_ram(
	input clk,reset,wr,rd,
	input [4:0] data_in,
	input [2:0] wr_addr,rd_addr,
	output reg [4:0] data_out);

integer i;

reg [4:0] mem [7:0];

always @(posedge clk)
begin
	if(reset)
	begin
		for(i=0;i<8;i=i+1)
		begin
			mem[i]<=0;
		end
	end

	else
	begin

		if(wr)
			mem[wr_addr]<=data_in;

		if(rd)
			data_out<=mem[rd_addr];
	end
end
endmodule
