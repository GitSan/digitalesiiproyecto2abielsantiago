////////////////////////////////////////////////////////////////////////////////
//Definición módulo demux 2:1 con selector automático
////////////////////////////////////////////////////////////////////////////////
`timescale 1ns /100ps
module demux_5bits (
	input clk_f0,
	input clk_2f0,
	input reset_L,
	output [4:0] data_out_0,
	output valid_data_out_0,
	output valid_data_out_1,
	output [4:0] data_out_1,
	input valid_data_in,
	input [4:0] data_in);//Declaración de variables entrada-salida


assign data_out_0 = (valid_data_in)?((clk_f0)?0:data_in):0;
assign data_out_1 = (valid_data_in)?((clk_f0)?data_in:0):0;
assign valid_data_out_0 = valid_data_in && !clk_f0;
assign valid_data_out_1 = valid_data_in && clk_f0;

/*always @(*) begin
	if(valid_data_in) begin
		if(clk_f0) begin
			data_out_1 = data_in;
			valid_data_out_1 = valid_data_in;
		end
		else begin
			data_out_0 = data_in;
			valid_data_out_0 = valid_data_in;
		end	
	end
	else begin
		data_out_0 = 0;
		data_out_1 = 0;
		valid_data_out_0 = 0;
		valid_data_out_1 = 0;
	end
end*/
/*
    //Declaración de variables internas
    reg selector;
	reg [4:0] flop0_data_in, flop1_data_in, flop2_data_in;
	reg [4:0] flop0_valid_data_in, flop1_valid_data_in, flop2_valid_data_in;
	wire [4:0] flop0_data_out0, flop0_data_out1;
	reg [4:0] flop1_data_out1,flop1_data_out0,flop2_data_out0;
	wire [4:0] flop0_valid_data_out0, flop0_valid_data_out1;
	reg [4:0] flop1_valid_data_out1,flop1_valid_data_out0,flop2_valid_data_out0;
    reg	flop0_selector,	flop1_selector;
    
// by  2018-11-25 | 	assign selector = clk_f0;

	always @(posedge clk_2f0)
	begin
		if(reset_L)
			begin
			//Flops de datos de entrada
			flop0_data_in <= data_in;
			flop1_data_in <= flop0_data_in;
			flop2_data_in <= flop1_data_in;
			flop0_valid_data_in <= valid_data_in;
			flop1_valid_data_in <= flop0_valid_data_in;
			flop2_valid_data_in <= flop1_valid_data_in;
			flop0_selector <= clk_f0;
			flop1_selector <= flop0_selector;
			selector <= flop1_selector;

			//flops de sincronizacion
// by  2018-11-21 | 			flop1_data_out0 <= flop0_data_out0;
// by  2018-11-21 | 			flop2_data_out0 <= flop1_data_out0;
			flop1_data_out1 <= flop0_data_out1;
			flop1_valid_data_out0 <= flop0_valid_data_out0;
			flop2_valid_data_out0 <= flop1_valid_data_out0;
			flop1_valid_data_out1 <= flop0_valid_data_out1;


			//Flops de datos de salida
			data_out_0 <= flop0_data_out0;
			data_out_1 <= flop1_data_out1;
			valid_data_out_0 <= flop0_valid_data_out0;
			valid_data_out_1 <= flop1_valid_data_out1;
		end
		else
		begin
		    flop1_data_out1 <=0;
			data_out_0<=0;
			data_out_1<=0;
			flop1_valid_data_out1 <= 0;
			valid_data_out_0<=0;
			valid_data_out_1<=0;
			flop0_data_in <=0;
			flop1_data_in <=0;
			flop2_data_in <=0;
			flop0_valid_data_in <= 0;
			flop1_valid_data_in <= 0;
			flop2_valid_data_in <= 0;
		end
	end

    assign flop0_data_out0 = (!reset_L)?0:((selector)?data_out_0:flop2_data_in);
    assign flop0_data_out1 = (!reset_L)?0:((selector)?flop2_data_in:flop1_data_out1) ;
    assign flop0_valid_data_out0 = (!reset_L)?0:((selector)?0:flop2_valid_data_in);
    assign flop0_valid_data_out1 = (!reset_L)?0:((selector)?flop2_valid_data_in:0) ;

*/
endmodule
