`include "dual_ram.v"
module fifo (
	input [3:0] data_in,			//dato entrada de 4 bits	
	input dest_in,					//quinto bit del dato entrada, entrada destino
	input clk, reset, push, pop,			//reloj, reset, write, read
	input [2:0] umbral_almost_empty, 
	input [2:0] umbral_almost_full,
	output fifo_full, fifo_empty,	//señales de salida de lleno y vacío
	output fifo_almost_full_pause, fifo_almost_empty_continue, //señales de salida de pause y continue
	output fifo_error, //Si se hace escritura y no lectura cuando fifo está lleno.
	output reg [3:0] data_out,		//dato salida
	output reg dest_out);			//quinto bit del dato salida, salida destino

//donde write sera el push y read el pop

// by  2018-11-18 | 	reg [3:0] ram [0:7], cnt;		//ram = array de 8 palabras de 4bits c/u

	reg [2:0] wr_ptr, rd_ptr;
	reg push_mem, pop_mem;
	integer cnt;

	dual_ram mem (/*autoinst*/
		      // Outputs
		      .data_out		(data_out_mem[4:0]),
		      // Inputs
		      .clk		(clk),
		      .reset		(reset),
		      .wr		(push),
		      .rd		(pop),
		      .data_in		(data_in_mem[4:0]),
		      .wr_addr		(wr_ptr[2:0]),
		      .rd_addr		(rd_ptr[2:0]));

    wire [4:0] data_out_mem;
	reg [4:0] data_in_mem;

	//escritura
	always @(posedge clk) begin
	        data_in_mem[4:0] <= {dest_in,data_in[3:0]};
	end

	//lectura
	always @(posedge clk) begin
			{dest_out,data_out[3:0]} <= data_out_mem[4:0];
	end

	//punteros
	always @(posedge clk) begin
		if (reset) begin															//si se activa el reset:
			wr_ptr <= 0;														//puntero de escritura en cero.
			rd_ptr <= 0;														//puntero de lectura en cero.
		end
		else begin																//si el reset no se activo
			wr_ptr <= ((push_mem && !fifo_full)||(push_mem && pop_mem)) ? wr_ptr+1 : wr_ptr;		//si (se escribe y el fifo no esta lleno) O (se escribe y lee):
			rd_ptr <= ((pop_mem && !fifo_empty)||(push_mem && pop_mem)) ? rd_ptr+1 : rd_ptr;	//si (se lee y el fifo no esta vacio) O (se escribe y lee):
																				//se incrementa en uno el puntero de lectura SINO se mantiene.
		end
	end

	//contador para fifo_(full/empty)
	always @(posedge clk) begin
		if (reset) begin
			cnt <= 0;
		end
		else begin
			case ({push_mem,pop_mem})							//concatena los bits de escritura y lectura.
			2'b00 : cnt <= cnt;						//si no se lee ni escribe: se mantiene el contador.
			2'b01 : begin
			    if(!fifo_empty) cnt <= cnt-1;	//si solo se lee: si el contador era cero, lo mantienese SINO reduce en uno el contador.
				else cnt <= cnt;
			end
			2'b10 : begin 
			    if(!fifo_full) cnt <= cnt+1;	//si solo se escribe: si el contador era 8, lo mantiene SINO lo aumenta.
				else cnt <= cnt;
			end
			2'b11 : cnt <= cnt;						//si se lee Y escribe: se mantiene el contador.
			default: cnt <= cnt;
			endcase
		end
	end

    //Lógica push y pop a la memoria
// by  2018-11-18 |     always @(posedge clk)
    always @(*)
    begin
		pop_mem=0;
		push_mem=0;
		if (!reset) begin
			if(!fifo_full) push_mem=push;
			if(!fifo_empty) pop_mem=pop;
		end
    end

	assign fifo_full = (cnt == 8)?1:0;		//si el contador es ocho, pone un uno de lleno
	assign fifo_almost_full_pause = (cnt > umbral_almost_full)?1:0;		//si el contador es ocho, pone un uno de lleno
	assign fifo_almost_empty_continue = (cnt < umbral_almost_empty)?1:0;		//si el contador es ocho, pone un uno de lleno
	assign fifo_empty = (cnt == 0)?1:0;		//si el contador es cero, pone un uno de vacio
	assign fifo_error = ((push && (!pop)) && fifo_full )?1:0;

endmodule
