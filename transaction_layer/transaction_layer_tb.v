// +FHDR------------------------------------------------------------
//                 Copyright (c) 2018 .
//                       ALL RIGHTS RESERVED
// -----------------------------------------------------------------
// Filename      : transaction_layer_tb.v
// Author        : 
// Created On    : 2018-11-18 18:55
// Last Modified : 
// -----------------------------------------------------------------
// Description:
//
//
// -FHDR------------------------------------------------------------

`include "transaction_layer.v"
`include "transaction_layer_p.v"
`include "greloj_cond.v"
`include "transaction_layer_estructural.v"
`include "cmos_cells.v"

`timescale 1ns/100ps

module transaction_layer_tb();

    wire [5:0] umbral;
    wire [4:0] data_out0_c, data_out1_c;
    wire [4:0] data_out0_e, data_out1_e;
    wire [3:0] data_in0, data_in1;
    wire clk, clk_2f, clk_4f, clk_8f, clk_16f, rst, rst_clk, init;
	wire push_VC0E0, push_VC1E0, push_VC0E1, push_VC1E1;
	wire vc_id0, vc_id1, dest0, dest1;
	wire valid_data_in0, valid_data_in1;
	wire pause0_vc0_c, continue0_vc0_c, pause0_vc1_c, continue0_vc1_c, pause1_vc0_c, continue1_vc0_c, pause1_vc1_c, continue1_vc1_c;
	wire eror_C, active_c, idle_c;
	wire valid_data_out0_c, valid_data_out1_c;
	wire pause0_vc0_e, continue0_vc0_e, pause0_vc1_e, continue0_vc1_e, pause1_vc0_e, continue1_vc0_e, pause1_vc1_e, continue1_vc1_e;
	wire eror_e, active_e, idle_e;
	wire valid_data_out0_e, valid_data_out1_e;


    greloj_cond clks (/*autoinst*/
		      // Outputs
		      .clk_8		(clk_8f),
		      .clk_4		(clk_4f),
		      .clk_2		(clk_2f),
		      .clk_1		(clk),
		      // Inputs
		      .reset_L		(rst_clk),
		      .clk_in		(clk_16f));

    transaction_layer_p probador (/*autoinst*/
				  // Outputs
				  .clk			(clk_16f),
				  .rst			(rst),
				  .rst_clk		(rst_clk),
				  .push_VC0E0		(push_VC0E0),
				  .push_VC1E0		(push_VC1E0),
				  .push_VC0E1		(push_VC0E1),
				  .push_VC1E1		(push_VC1E1),
				  .umbral		(umbral[5:0]),
				  .init			(init),
				  .vc_id0		(vc_id0),
				  .vc_id1		(vc_id1),
				  .dest0		(dest0),
				  .dest1		(dest1),
				  .data_in0		(data_in0[3:0]),
				  .data_in1		(data_in1[3:0]),
				  .valid_data_in0	(valid_data_in0),
				  .valid_data_in1	(valid_data_in1),
				  // Inputs
				  .pause0_vc0_c		(pause0_vc0_c),
				  .continue0_vc0_c	(continue0_vc0_c),
				  .pause0_vc1_c		(pause0_vc1_c),
				  .continue0_vc1_c	(continue0_vc1_c),
				  .pause1_vc0_c		(pause1_vc0_c),
				  .continue1_vc0_c	(continue1_vc0_c),
				  .pause1_vc1_c		(pause1_vc1_c),
				  .continue1_vc1_c	(continue1_vc1_c),
				  .error_c		(error_c),
				  .active_c		(active_c),
				  .idle_c			(idle_c),
				  .data_out0_c		(data_out0_c[4:0]),
				  .data_out1_c		(data_out1_c[4:0]),
				  .valid_data_out0_c	(valid_data_out0_c),
				  .valid_data_out1_c	(valid_data_out1_c),
				  .pause0_vc0_e		(pause0_vc0_e),
				  .continue0_vc0_e	(continue0_vc0_e),
				  .pause0_vc1_e		(pause0_vc1_e),
				  .continue0_vc1_e	(continue0_vc1_e),
				  .pause1_vc0_e		(pause1_vc0_e),
				  .continue1_vc0_e	(continue1_vc0_e),
				  .pause1_vc1_e		(pause1_vc1_e),
				  .continue1_vc1_e	(continue1_vc1_e),
				  .error_e		(error_e),
				  .active_e		(active_e),
				  .idle_e			(idle_e),
				  .data_out0_e		(data_out0_e[4:0]),
				  .data_out1_e		(data_out1_e[4:0]),
				  .valid_data_out0_e	(valid_data_out0_e),
				  .valid_data_out1_e	(valid_data_out1_e));

    transaction_layer tl_c (/*autoinst*/
			    // Outputs
			    .pause0_vc0		(pause0_vc0_c),
			    .continue0_vc0	(continue0_vc0_c),
			    .pause0_vc1		(pause0_vc1_c),
			    .continue0_vc1	(continue0_vc1_c),
			    .pause1_vc0		(pause1_vc0_c),
			    .continue1_vc0	(continue1_vc0_c),
			    .pause1_vc1		(pause1_vc1_c),
			    .continue1_vc1	(continue1_vc1_c),
			    .error		(error_c),
			    .active		(active_c),
			    .idle		(idle_c),
			    .data_out0		(data_out0_c[4:0]),
			    .data_out1		(data_out1_c[4:0]),
			    .valid_data_out0	(valid_data_out0_c),
			    .valid_data_out1	(valid_data_out1_c),
			    // Inputs
			    .clk		(clk_8f),
			    .clk_2f		(clk_16f),
			    .rst		(rst),
			    .push_VC0E0		(push_VC0E0),
			    .push_VC1E0		(push_VC1E0),
			    .push_VC0E1		(push_VC0E1),
			    .push_VC1E1		(push_VC1E1),
			    .umbral		(umbral[5:0]),
			    .init		(init),
			    .vc_id0		(vc_id0),
			    .vc_id1		(vc_id1),
			    .dest0		(dest0),
			    .dest1		(dest1),
			    .data_in0		(data_in0[3:0]),
			    .data_in1		(data_in1[3:0]),
			    .valid_data_in0	(valid_data_in0),
			    .valid_data_in1	(valid_data_in1));

    transaction_layer_estructural tl_e (/*autoinst*/
			    // Outputs
			    .pause0_vc0		(pause0_vc0_e),
			    .continue0_vc0	(continue0_vc0_e),
			    .pause0_vc1		(pause0_vc1_e),
			    .continue0_vc1	(continue0_vc1_e),
			    .pause1_vc0		(pause1_vc0_e),
			    .continue1_vc0	(continue1_vc0_e),
			    .pause1_vc1		(pause1_vc1_e),
			    .continue1_vc1	(continue1_vc1_e),
			    .error		(error_e),
			    .active		(active_e),
			    .idle		(idle_e),
			    .data_out0		(data_out0_e[4:0]),
			    .data_out1		(data_out1_e[4:0]),
			    .valid_data_out0	(valid_data_out0_e),
			    .valid_data_out1	(valid_data_out1_e),
			    // Inputs
			    .clk		(clk_8f),
			    .clk_2f		(clk_16f),
			    .rst		(rst),
			    .push_VC0E0		(push_VC0E0),
			    .push_VC1E0		(push_VC1E0),
			    .push_VC0E1		(push_VC0E1),
			    .push_VC1E1		(push_VC1E1),
			    .umbral		(umbral[5:0]),
			    .init		(init),
			    .vc_id0		(vc_id0),
			    .vc_id1		(vc_id1),
			    .dest0		(dest0),
			    .dest1		(dest1),
			    .data_in0		(data_in0[3:0]),
			    .data_in1		(data_in1[3:0]),
			    .valid_data_in0	(valid_data_in0),
			    .valid_data_in1	(valid_data_in1));

	integer idx;
	initial begin
		$dumpfile("transaction_layer.vcd");
        $dumpvars;
        for (idx = 0; idx < (8); idx = idx + 1) begin
			$dumpvars(1,tl_c.fifoVC0E0.mem.mem[idx]);
			$dumpvars(1,tl_c.fifoVC1E0.mem.mem[idx]);
			$dumpvars(1,tl_c.fifoVC0E1.mem.mem[idx]);
			$dumpvars(1,tl_c.fifoVC1E1.mem.mem[idx]);
        end
    end

endmodule
