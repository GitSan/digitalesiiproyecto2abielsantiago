////////////////////////////////////////////////////////////////////////////////
//Definición modulo de verificación
////////////////////////////////////////////////////////////////////////////////
`timescale 1ns / 100ps
module fsm_p (
	output reg clk,
	output reg reset,
	output reg init,
	output reg [3:0] fifo_empty,
	output reg [3:0] fifo_error, 
	output reg [5:0] umbral,
	input [5:0] umbrales_c, 
	input idle_c,
	input active_c,
	input error_c,
	input [5:0] umbrales_e, 
	input idle_e,
	input active_e,
	input error_e);

	//Reloj
	initial 
	begin
		clk <= 0;
	end

	always 
	begin
		#1 clk <= ~clk;
	end

    //Declaración de variables internas
    reg compare;
    reg [8:0] conductual, estructural;

	//Pruebas
	initial 
	begin
	    $dumpfile("fsm.vcd");
        $dumpvars;
		@(posedge clk);
		reset<=1;
		init<=0;
		fifo_empty <= 4'b1111;
		fifo_error <= 4'b0000;
		umbral <= 6'b000000;
		@(posedge clk);@(posedge clk);
		reset<=0;
		@(posedge clk);@(posedge clk);
		init<=1;
		umbral <= 6'b110100; //MSB:110 = almost full, LSB:100 = almost empty.
		@(posedge clk);@(posedge clk);
		init<=0;
		#10 @(posedge clk);@(posedge clk);
		fifo_empty <= 4'b1101;
		umbral <= 6'b000000;
		#10 @(posedge clk);@(posedge clk);
		fifo_error <= 4'b1000; //4'b[VC0E0,VC1E0,VC0E1,VC1E1]
		#10 @(posedge clk);@(posedge clk);
		reset<=1;
		@(posedge clk);@(posedge clk);
		reset<=0;
		init<=1;
		fifo_empty <= 4'b1111;
		fifo_error <= 4'b0000;
		umbral <= 6'b0;
		#20	$finish;
	end

	//Asignación de salidas de los multiplexores a registros internos de probador.v para comparación
    always @(posedge clk)
    begin
        conductual[8:0] <= {idle_c,active_c,error_c,umbrales_c[5:0]};
        estructural[8:0] <= {idle_e,active_e,error_e,umbrales_e[5:0]};
    end
    //Lógica de comparación
    always @(*)
    begin
        compare = 0;
        if(conductual != estructural)
        begin
            compare = 1;
        end
    end

endmodule
