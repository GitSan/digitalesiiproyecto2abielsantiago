// by  2018-11-17 | `include "fsm.v"
// by  2018-11-17 | `include "fsm_p.v"
`include "fsm_estructural.v"
`include "cmos_cells.v"
`timescale 1ns/100ps
module fsm_tb ();

    wire clk, reset, init, idle_c, active_c, error_c, idle_e, active_e, error_e;
	wire [5:0] umbral, umbrales_c, umbrales_e;
	wire [3:0] fifo_empty, fifo_error;

    fsm_p fsm_p_0 (/*autoinst*/
		   // Outputs
		   .clk			(clk),
		   .reset		(reset),
		   .init		(init),
		   .fifo_empty		(fifo_empty[3:0]),
		   .fifo_error		(fifo_error[3:0]),
		   .umbral		(umbral[5:0]),
		   // Inputs
		   .umbrales_c		(umbrales_c[5:0]),
		   .idle_c		(idle_c),
		   .active_c		(active_c),
		   .error_c		(error_c),
		   .umbrales_e		(umbrales_e[5:0]),
		   .idle_e		(idle_e),
		   .active_e		(active_e),
		   .error_e		(error_e));

    fsm fsm_cond_0 (/*autoinst*/
		    // Outputs
		    .umbrales		(umbrales_c[5:0]),
		    .idle		(idle_c),
		    .active		(active_c),
		    .error		(error_c),
		    // Inputs
		    .clk		(clk),
		    .reset		(reset),
		    .init		(init),
		    .fifo_empty		(fifo_empty[3:0]),
		    .fifo_error		(fifo_error[3:0]),
		    .umbral		(umbral[5:0]));

    fsm_estructural fsm_estr_0 (/*autoinst*/
		    // Outputs
		    .umbrales		(umbrales_e[5:0]),
		    .idle		(idle_e),
		    .active		(active_e),
		    .error		(error_e),
		    // Inputs
		    .clk		(clk),
		    .reset		(reset),
		    .init		(init),
		    .fifo_empty		(fifo_empty[3:0]),
		    .fifo_error		(fifo_error[3:0]),
		    .umbral		(umbral[5:0]));

endmodule
