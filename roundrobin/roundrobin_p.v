// +FHDR------------------------------------------------------------
//                 Copyright (c) 2018 .
//                       ALL RIGHTS RESERVED
// -----------------------------------------------------------------
// Filename      : roundrobin_p.v
// Author        : 
// Created On    : 2018-11-14 18:00
// Last Modified : 
// -----------------------------------------------------------------
// Description:
//
//
// -FHDR------------------------------------------------------------

`timescale 1ns/100ps

module roundrobin_p(
    output reg clk,
	output reg request0,
	output reg request1,
	input valid_c,
	input id_c,
	input valid_e,
	input id_e
);


	//Reloj
	initial clk <= 0;
	always #1 clk <= ~clk;

    //Declaración de variables internas                                                                                                                                                                     
    reg compare;
    reg conductual, estructural;

    initial begin
        $dumpfile("rr.vcd");
        $dumpvars;
	    @(posedge clk);
		request0 = 0;
		request1 = 0;
	    #10 @(posedge clk);
		request0 = 1;
	    #10 @(posedge clk);
		request1 = 1;
	    #10 @(posedge clk);
		request0 = 0;
	    #10 @(posedge clk);
		request1 = 0;
	    #10 @(posedge clk);
		request0 = 1;
		request1 = 1;
	    #10 @(posedge clk);
		request0 = 0;
		request1 = 0;
		#10 $finish;
	end

	//Asignación de salidas de los multiplexores a registros internos de probador.v para comparación
    always @(posedge clk)
    begin
        conductual <= valid_c;
        estructural <= valid_e;
    end
    //Lógica de comparación
    always @(*)
    begin
        compare = 0;
        if(conductual != estructural)
        begin
            compare = 1;
        end
    end


endmodule
