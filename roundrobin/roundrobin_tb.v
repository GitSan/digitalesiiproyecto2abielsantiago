// +FHDR------------------------------------------------------------
//                 Copyright (c) 2018 .
//                       ALL RIGHTS RESERVED
// -----------------------------------------------------------------
// Filename      : roundrobin_tb.v
// Author        : 
// Created On    : 2018-11-14 18:07
// Last Modified : 
// -----------------------------------------------------------------
// Description:
//
//
// -FHDR------------------------------------------------------------
`include "roundrobin.v"
`include "roundrobin_p.v"
`include "rr_estructural_auto_mapped.v"
`include "cmos_cells.v"
`timescale 1ns/100ps

module roundrobin_tb();

    wire clk, request0, request1, valid_c, id_c,valid_e, id_e;

    roundrobin_p rr_p0 (/*autoinst*/
			// Outputs
			.clk		(clk),
			.request0	(request0),
			.request1	(request1),
			// Inputs
			.valid_c		(valid_c),
			.id_c		(id_c),
			.valid_e		(valid_e),
			.id_e		(id_e));

    roundrobin rr_cond_0 (/*autoinst*/
		    // Outputs
		    .valid		(valid_c),
		    .id			(id_c),
		    // Inputs
		    .clk		(clk),
		    .request0		(request0),
		    .request1		(request1));

	rr_estructural_auto_mapped rr_estr_0 (/*autoinst*/
             // Outputs
              .valid      (valid_e),
              .id         (id_e),
              // Inputs
              .clk        (clk),
              .request0       (request0),
              .request1       (request1));

endmodule
